mkdir -p build
mkdir -p test/build
gcc -O3 -c ./src/slurper.c -o ./build/slurper.o
if [ ! -f ./build/yyjson.o ]; then
    gcc -O3 -c ./src/yyjson/src/yyjson.c -o ./build/yyjson.o
fi
gcc -O3 -c ./src/utils.c -o ./build/utils.o
gcc -O3 -c ./src/script.c -o ./build/script.o
gcc -O3 ./test/src/test_slurper.c ./build/utils.o ./build/script.o ./build/yyjson.o ./build/slurper.o -l curl -o ./test/build/test_slurper
gcc -O3 ./test/src/test_utils.c ./build/utils.o -o ./test/build/test_utils
gcc -O3 ./test/src/test_script.c ./build/utils.o ./build/script.o -o ./test/build/test_script
