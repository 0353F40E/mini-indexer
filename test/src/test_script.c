#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "../../src/utils.h"
#include "../../src/script.h"

int main(void)
{
    {
        char bytecode1[] = "40da2963cc172e7dccf9570ebd272c496d9df459f1b4d07f1961515642054db764f25c4aab947a4dbcf7793ca25bcc5a46faa83d93e7aeaa2e23a95376e386902d10020aa262ab330100863301002b45000040c0df593545220c40b8676d56388b58715b27cc0fc5de3640dfd49aa05d00e3efc5597b9d8dd783f055b0579630157599290d547e9e3c160d2170d2c300177a72103e0aa262ac3301008733010026450000514d5401043e0aa262041209a26203eab30202e53303bc6d390500743ba40b2102d3c1de9d4bc77d6c3608cbe44d10138c7488e592dc2b1e10a6cf0e92c2ecb0471976a91415d54e1b90d806548f34263afe71695a8a19716388ac1976a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac51210374059eb4b0edb9052779a1ef93c76d1715473a9f3d6634135a3c03b82561a015210396dc6749e3bde2c230fb10bb66a444d83318521c449181a6be57123c1257a6575c79009c637b695c7a7cad5b7a7cad6d6d6d6d6d51675c7a519dc3519d5f7a5f795779bb5d7a5d79577abb5c79587f77547f75817600a0695c79587f77547f75818c9d5c7a547f75815b799f695b795c7f77817600a0695979a35879a45c7a547f7581765c7aa2695b7aa2785a7a8b5b7aa5919b6902220276587a537a96a47c577a527994a4c4529d00cc7b9d00cd557a8851cc9d51cd547a8777777768";
        char bytecode1bin[2001];
        char bytecode1binhex[2001];
        char parsed[2001];
        char parsedh[2001];
        size_t plen;
        size_t bytes;
        bytecode1bin[2000]='\0';
        parsed[2000]='\0';
        parsedh[2000]='\0';
        printf("ExtractBytecodePattern Tests\n");
        printf("bytecode1      : %s\n", bytecode1);

        bytes = Hex2Bin(bytecode1bin, bytecode1, strlen(bytecode1));
        Bin2Hex(bytecode1binhex, bytecode1bin, bytes);
        printf("BYTECODE:        %s; len=%zu\n", bytecode1binhex, bytes);

        plen = ExtractBytecodePattern(parsed, bytecode1bin, bytes, STRIP_PUSHES);
        Bin2Hex(parsedh, parsed, plen);
        printf("STRIP_PUSHES:    %s; len=%zu\n", parsedh, plen);

        plen = ExtractBytecodePattern(parsed, bytecode1bin, bytes, STRIP_PUSH_DATA);
        Bin2Hex(parsedh, parsed, plen);
        printf("STRIP_PUSH_DATA: %s; len=%zu\n", parsedh, plen);

        plen = ExtractBytecodePattern(parsed, bytecode1bin, bytes, EXTRACT_PUSHES);
        Bin2Hex(parsedh, parsed, plen);
        printf("EXTRACT_PUSHES:  %s; len=%zu\n", parsedh, plen);

        printf("CountBytecodePushes Tests\n");

        plen = CountBytecodePushes(bytecode1bin, bytes);
        printf("CountBytecodePushes: %zu\n", plen);

        printf("GetBytecodePushedData Tests\n");

        size_t ind=0;
        plen = GetBytecodePushedData(parsed, bytecode1bin, bytes, ind, 0);
        Bin2Hex(parsedh, parsed, plen);
        printf("PUSH N. %4zu:        %s; len=%zu\n", ind, parsedh, plen);

        ind=1;
        plen = GetBytecodePushedData(parsed, bytecode1bin, bytes, ind, 1);
        Bin2Hex(parsedh, parsed, plen);
        printf("PUSH N.-%4zu:        %s; len=%zu\n", ind, parsedh, plen);

        ind=0;
        plen = GetBytecodePushedData(parsed, bytecode1bin, bytes, ind, 1);
        Bin2Hex(parsedh, parsed, plen);
        printf("PUSH N.-%4zu:        %s; len=%zu\n", ind, parsedh, plen);
    }
}
