#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "../../src/utils.h"

int main(void)
{
    {
        printf("Bin2Hex Tests\n");
        uint8_t u8 = 0x11u;
        uint16_t u16 = 0x11ffu;
        uint32_t u32 = 0x1122ff44u;
        uint64_t u64 = 0x11223344ff667788u;
        uint64_t u64x2[2];
        char c3[3] = {0x11, 0xff, 0x33};
        u64x2[0] = u64;
        u64x2[1] = u64;
        char str[33];
        Bin2Hex(str, (char*)&u8, 1);
        printf("%s\n", str);
        Bin2Hex(str, (char*)&u16, 2);
        printf("%s\n", str);
        Bin2Hex(str, (char*)&u32, 4);
        printf("%s\n", str);
        Bin2Hex(str, (char*)&u64, 8);
        printf("%s\n", str);
        Bin2Hex(str, (char*)u64x2, 16);
        printf("%s\n", str);
        Bin2Hex(str, c3, 3);
        printf("%s\n", str);
        printf("\n");
    }
    {
        printf("Hex2Bin Tests\n");
        char u64x2[] = "887766ff443322118877665544332211";
        char u64x2b[] = "887766ff443x22118877665544332211";
        char dst1[16];
        char dst2[16];
        size_t len;
        printf("%s\n", u64x2);
        printf("%s\n", u64x2b);
        len = Hex2Bin(dst1, u64x2, 32);
        printf("len=%zu; hex=", len);
        for (size_t i=0; i < len; i++)
            printf("%02x", (uint8_t)dst1[i]);
        printf("\n");
        len = Hex2Bin(dst2, u64x2b, 32);
        printf("len=%zu; hex=", len);
        for (size_t i=0; i < len; i++)
            printf("%02x", (uint8_t)dst2[i]);
        printf("\n");
        printf("\n");
        printf("ReverseBytesHex Tests\n");
        ReverseBytesHex(u64x2, u64x2, 32);
        printf("%s\n", u64x2);
        ReverseBytesHex(u64x2b, u64x2b, 10);
        printf("%s\n", u64x2b);
        printf("ReverseBytesBin Tests\n");
        len=16;
        ReverseBytesBin(dst1, dst1, len);
        for (size_t i=0; i < len; i++)
            printf("%02x", (uint8_t)dst1[i]);
        printf("\n");
        len=5;
        ReverseBytesBin(dst2, dst2, len);
        for (size_t i=0; i < len; i++)
            printf("%02x", (uint8_t)dst2[i]);
        printf("\n");
    }
    {
        printf("Bin2IntLE Tests\n");
        char u1h[] = "ff";
        char u2h[] = "ffff";
        char u4h[] = "ffffffff";
        char u1b[1];
        char u2b[2];
        char u4b[4];
        uint64_t u1i;
        uint64_t u2i;
        uint64_t u4i;
        Hex2Bin(u1b, u1h, 2);
        Hex2Bin(u2b, u2h, 4);
        Hex2Bin(u4b, u4h, 8);
        u1i = Bin2UIntLE(u1b, 1);
        u2i = Bin2UIntLE(u2b, 2);
        u4i = Bin2UIntLE(u4b, 4);
        printf("u1=%lu; u2=%lu; u4=%lu\n", u1i, u2i, u4i);
        printf("u1=%lx; u2=%lx; u4=%lx\n", u1i, u2i, u4i);
    }
    {
        printf("Int2BinScriptNum Tests\n");
        char snum[8];
        char snumh[17];
        size_t sz;
        int64_t num = 80000000;
        sz = Int2BinScriptNum(snum, num);
        Bin2Hex(snumh, snum, sz);
        printf("num=%ld; numh=%lx; sz=%zu;\n", num, num, sz);
        printf("num=%ld; numh=%lx; sz=%zu; snum=%s\n", num, num, sz, snumh);
    }
}
