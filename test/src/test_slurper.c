#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <time.h>
#include <assert.h>
#include "../../src/slurper.h"
#include <pthread.h>
#include "../../src/yyjson/src/yyjson.h"
#include "../../src/utils.h"
#include "../../src/script.h"

typedef struct {
    Slurper *slurper;
    size_t blockHeight;
    yyjson_doc *jsonDoc;
    size_t jsmnCountTokens;
} SlurperJob;

SlurperJob* SlurperJobNew(size_t blockHeight)
{
    Slurper *slurper = SlurperNew();
    if(slurper == NULL) {
        fprintf(stderr, "Slurper *slurper = SlurperNew(); failed.\n");
        return NULL;
    }
    SlurperJob *slurperJob = (SlurperJob *)malloc(sizeof(SlurperJob));
    if(slurperJob == NULL) {
        fprintf(stderr, "SlurperJob *slurperJob = malloc(sizeof(SlurperJob); failed.\n");
        SlurperDestroy(slurper);
        return NULL;
    }
    slurperJob->slurper = slurper;
    slurperJob->blockHeight = blockHeight;
    return slurperJob;
}

void SlurperJobSet(SlurperJob* slurperJob, size_t blockHeight)
{
    slurperJob->blockHeight = blockHeight;
    yyjson_doc_free(slurperJob->jsonDoc);
}

void SlurperJobDestroy(SlurperJob *slurperJob)
{
    SlurperDestroy(slurperJob->slurper);
    yyjson_doc_free(slurperJob->jsonDoc);
    free(slurperJob);
    slurperJob = NULL;
}

void* SlurperJobRun(void *slurperJob)
{
    SlurperJob *sJob = (SlurperJob*)slurperJob;
    if (SlurperSlurp(sJob->slurper, sJob->blockHeight, 3) == 0) {
        sJob->jsonDoc = yyjson_read(sJob->slurper->block->m_blockDataJSON,
                                sJob->slurper->block->m_blockDataJSONLength,
                                0);
        return (void*)sJob;
    }
    else {
        return NULL;
    }
}

#define HEIGHT_START 173805 // p2sh activation 173805
#define HEIGHT_END 780000
#define THREADS 4

int main(void)
{
    CURLcode curlResult;
    curlResult = curl_global_init(CURL_GLOBAL_DEFAULT);
    if(curlResult != CURLE_OK) {
        fprintf(stderr, "curlResult = curl_global_init(CURL_GLOBAL_DEFAULT); failed: %s\n", curl_easy_strerror(curlResult));
        return 1;
    }

    pthread_t tid[THREADS];
    SlurperJob* slurperJob[THREADS];
    char *bytecodeBufferHex = malloc(2*1024*1024+1);
    char *bytecodeBufferBin = malloc(1*1024*1024);
    char *bytecodePatternBufferBin = malloc(1*1024*1024);
    char *bytecodePatternBufferHex = malloc(2*1024*1024+1);
    size_t bbLen;

    size_t t = 0;
    size_t pop_next = 0;
    size_t i = HEIGHT_START;

    for (t = 0; t<THREADS && t<(HEIGHT_END-i); t++) {
        slurperJob[t] = SlurperJobNew(t+i);
    }
    for (t = 0; t<THREADS && t<(HEIGHT_END-i); t++) {
        int error = pthread_create(&tid[t], NULL, SlurperJobRun, slurperJob[t]);
        if(0 != error)
          fprintf(stderr, "Couldn't run thread number %zu, errno %d\n", t, error);
    }
    i += t;
    while (t > 0) {
        pthread_join(tid[pop_next], NULL);
        if (slurperJob[pop_next]->blockHeight % 1000 == 0) {
            fprintf(stderr, "H=%lu\n", slurperJob[pop_next]->slurper->block->m_blockHeight);
        }
        //printf("%s", slurperJob[pop_next]->slurper->block->m_blockDataJSON);
        yyjson_doc *doc = slurperJob[pop_next]->jsonDoc;
        yyjson_val *root = yyjson_doc_get_root(doc);
        yyjson_val *hits = yyjson_obj_get(root, "result");
        hits = yyjson_obj_get(hits, "tx");
        size_t idx, max;
        yyjson_val *hit;
        yyjson_arr_foreach(hits, idx, max, hit) {
            yyjson_val *txid = yyjson_obj_get(hit, "txid");
            //printf("%s,%lu,%lu\n", yyjson_get_str(txid), slurperJob[pop_next]->slurper->block->m_blockHeight, idx);
            size_t vidx, vmax;
            yyjson_val *vins = yyjson_obj_get(hit, "vin");
            yyjson_val *vin;
            yyjson_arr_foreach(vins, vidx, vmax, vin) {
                yyjson_val *tmp;
                tmp = yyjson_obj_get(vin, "prevout");
                tmp = yyjson_obj_get(tmp, "scriptPubKey");
                tmp = yyjson_obj_get(tmp, "hex");
                size_t tmpl = yyjson_get_len(tmp);
                tmpl = Hex2Bin(bytecodeBufferBin, yyjson_get_str(tmp), tmpl);
                tmpl = ExtractBytecodePattern(bytecodePatternBufferBin, bytecodeBufferBin, tmpl, STRIP_PUSHES);
                tmpl = Bin2Hex(bytecodePatternBufferHex, bytecodePatternBufferBin, tmpl);
                if (memcmp("a95187", bytecodePatternBufferHex, 6) == 0) {
                    tmp = yyjson_obj_get(vin, "scriptSig");
                    tmp = yyjson_obj_get(tmp, "hex");
                    tmpl = yyjson_get_len(tmp);
                    tmpl = Hex2Bin(bytecodePatternBufferBin, yyjson_get_str(tmp), tmpl);
                    tmpl = GetBytecodePushedData(bytecodeBufferBin, bytecodePatternBufferBin, tmpl, 0, 1);
                    tmpl = ExtractBytecodePattern(bytecodePatternBufferBin, bytecodeBufferBin, tmpl, STRIP_PUSHES);
                    tmpl = Bin2Hex(bytecodePatternBufferHex, bytecodePatternBufferBin, tmpl);
                    printf("%s\n", bytecodePatternBufferHex);
                }
            }
        }
        pop_next = (pop_next + 1) % THREADS;
        if (i < HEIGHT_END) {
            int nt = (pop_next - 1) % THREADS;
            SlurperJobSet(slurperJob[nt], i);
            int error = pthread_create(&tid[nt], NULL, SlurperJobRun, slurperJob[nt]);
            if(0 != error) {
                fprintf(stderr, "Couldn't run thread number %zu, errno %d\n", t, error);
            }
            i++;
        }
        else {
            t--;
        }
    }

    for (t = 0; t < THREADS && t < HEIGHT_END - HEIGHT_START; t++) {
        SlurperJobDestroy(slurperJob[t]);
    }
    curl_global_cleanup();
    return 0;
}
