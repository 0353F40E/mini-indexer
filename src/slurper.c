#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <time.h>
#include <assert.h>
#include "./slurper.h"

// setup for getblockhash RPC method
static const size_t RPC_QUERY_SIZE_GETBLOCKHASH = (sizeof(RPC_QUERY_HEADER_GETBLOCKHASH) - 1) + 20 + (sizeof(RPC_QUERY_FOOTER_GETBLOCKHASH) - 1) + 1;
void BuildRPCQueryGetblockhash(char query[], size_t height)
{
    query[0]='\0';
    // header
    query = strcat(query, RPC_QUERY_HEADER_GETBLOCKHASH);
    // height
    char heightString[21];
    snprintf(heightString, 21, "%zu", height);
    query = strcat(query, heightString);
    // footer
    query = strcat(query, RPC_QUERY_FOOTER_GETBLOCKHASH);
}

// setup for getblock RPC method
static const size_t RPC_QUERY_SIZE_GETBLOCK = (sizeof(RPC_QUERY_HEADER_GETBLOCK) - 1) + 64 + (sizeof(RPC_QUERY_SEPARATOR_GETBLOCK) - 1) + 1 + (sizeof(RPC_QUERY_FOOTER_GETBLOCK) - 1) + 1;
void BuildRPCQueryGetblock(char query[], char blockHash[], int verbosity)
{
    query[0]='\0';
    // header
    query = strcat(query, RPC_QUERY_HEADER_GETBLOCK);
    // hash
    query = strcat(query, blockHash);
    // arg separator
    query = strcat(query, RPC_QUERY_SEPARATOR_GETBLOCK);
    // verbosity
    char verbosityString[2];
    snprintf(verbosityString, 2, "%d", verbosity);
    query = strcat(query, verbosityString);
    // footer
    query = strcat(query, RPC_QUERY_FOOTER_GETBLOCK);
}

Block* BlockNew(size_t blockDataInitialCapacity)
{
    Block* block = (Block *)malloc(sizeof(Block));
    if (block == NULL) {
        fprintf(stderr, "Block* block = malloc(sizeof(Block)); failed.\n");
    }
    else {
        // blockhash
        block->m_blockHash[0] = '\0';
        block->m_blockHashJSONLength = 0;
        block->m_maxBlockHashJSONLength = RPC_MAX_RESULT_SIZE_GETBLOCKHASH;
        // block
        block->m_blockDataJSONLength = 0;
        block->m_blockDataJSONCapacity = blockDataInitialCapacity;
        block->m_blockDataJSON = (char *)malloc(blockDataInitialCapacity);
        if (block->m_blockDataJSON == NULL) {
            fprintf(stderr, "block->m_blockDataJSON = malloc(maxBlockDataJSONLength); failed.\n");
            free(block);
            block = NULL;
        }
    }
    return block;
}

void BlockDestroy(Block *block)
{
    if (block != NULL) {
        free(block->m_blockDataJSON);
        free(block);
        block = NULL;
    }
}

int BlockUpdateHash(Block *block)
{
    if (memcmp("{\"result\":\"", block->m_blockHashJSON, 11) != 0 && block->m_blockHashJSON[76] != '\"') {
        fprintf(stderr, "RPC getblockhash failed to return block hash. RPC result:\n");
        fprintf(stderr, "%s\n", block->m_blockHashJSON);
        return 1;
    }
    memcpy(block->m_blockHash, &block->m_blockHashJSON[11], 64);
    block->m_blockHash[64] = '\0';
    return 0;
}

void BlockReset(Block *p)
{
    p->m_blockHash[0] = '\0';
    p->m_blockHashJSONLength = 0;
    p->m_blockDataJSONLength = 0;
}

size_t WriteHash(char *curlData, size_t curlSize, size_t curlCount, void *block)
{
    size_t curlBytes = curlSize * curlCount;
    Block *writeInto = (Block *)block;
    if (writeInto->m_blockHashJSONLength + curlBytes + 1 > writeInto->m_maxBlockHashJSONLength) {
        fprintf(stderr, "WriteRPCResult() failed: m_maxBlockHashJSONLength too small.\n");
        return 0;
    }
    memcpy(&(writeInto->m_blockHashJSON[writeInto->m_blockHashJSONLength]), curlData, curlBytes);
    writeInto->m_blockHashJSONLength += curlBytes;
    writeInto->m_blockHashJSON[writeInto->m_blockHashJSONLength] = '\0';
    return curlBytes;
}

size_t WriteBlock(char *curlData, size_t curlSize, size_t curlCount, void *block)
{
    size_t curlBytes = curlSize * curlCount;
    Block *writeInto = (Block *)block;
    const size_t capNeeded = writeInto->m_blockDataJSONLength + curlBytes + 1;
    if (capNeeded > writeInto->m_blockDataJSONCapacity) {
        const size_t capPlusChunk = writeInto->m_blockDataJSONCapacity + RPC_ALLOC_CHUNK_SIZE_GETBLOCK;
        const size_t newCap = capNeeded > capPlusChunk ? capNeeded : capPlusChunk;
        void *ptr = realloc(writeInto->m_blockDataJSON, newCap);
        if (!ptr) {
            fprintf(stderr, "WriteBlock() failed: unable to allocate additional capacity.\n");
            return 0;
        }
        writeInto->m_blockDataJSON = (char *)ptr;
        writeInto->m_blockDataJSONCapacity = newCap;
    }
    // why is this 2-3x faster than memcpy (at verbosity=0) ???
    // Calin: It shouldn't be. You are not optimizing with -O3 is my guess.
    for (size_t i=0; i<curlBytes; i++) {
        writeInto->m_blockDataJSON[writeInto->m_blockDataJSONLength + i] = curlData[i];
    }
    // for testing: memcpy is 2-3x slower, fwrite direct to stdout is about the same
    // memcpy(writeInto->m_blockDataJSON + writeInto->m_blockDataJSONLength, curlData, curlBytes);
    // fwrite(curlData, curlSize, curlCount, stdout);
    writeInto->m_blockDataJSONLength += curlBytes;
    writeInto->m_blockDataJSON[writeInto->m_blockDataJSONLength] = '\0';
    return curlBytes;
}

typedef size_t (*WriteCallback_t)(char *, size_t, size_t, void *);

CURL* SetupCurl(const char *rpcUrl, WriteCallback_t WriteCallback, void *writeInto, size_t curlBufferSize)
{
    CURL *curl;
    curl = curl_easy_init();
    if(curl == NULL) {
        fprintf(stderr, "CURL *curl = curl_easy_init(); failed\n");
    }
    else {
        curl_easy_setopt(curl, CURLOPT_URL, rpcUrl);
        curl_easy_setopt(curl, CURLOPT_POST, 1L);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_BUFFERSIZE, curlBufferSize);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, writeInto);
    }
    return curl;
}

Slurper* SlurperNew()
{
    Slurper* slurper = (Slurper *)malloc(sizeof(Slurper));
    if (slurper == NULL) {
        fprintf(stderr, "Slurper* slurper = (Slurper *)malloc(sizeof(Slurper)); failed.\n");
    }
    else {
        // initialize block cache
        slurper->block = BlockNew(RPC_ALLOC_CHUNK_SIZE_GETBLOCK);
        if (slurper->block == NULL) {
            fprintf(stderr, "slurper->block = BlockNew(RPC_ALLOC_CHUNK_SIZE_GETBLOCK); failed\n");
            free(slurper);
            slurper = NULL;
        }
        else {
            // blockhash
            slurper->curlBlockHash = SetupCurl(RPC_URL, WriteHash, slurper->block, RPC_CURL_BUFFER_SIZE_GETBLOCKHASH);
            if (slurper->curlBlockHash == NULL) {
                BlockDestroy(slurper->block);
                free(slurper);
                slurper = NULL;
            }
            else {
                // block
                slurper->curlBlockData = SetupCurl(RPC_URL, WriteBlock, slurper->block, RPC_CURL_BUFFER_SIZE_GETBLOCK);
                if (slurper->curlBlockData == NULL) {
                    curl_easy_cleanup(slurper->curlBlockHash);
                    BlockDestroy(slurper->block);
                    free(slurper);
                    slurper = NULL;
                }
            }
        }
    }
    return slurper;
}

void SlurperDestroy(Slurper *slurper)
{
    curl_easy_cleanup(slurper->curlBlockHash);
    curl_easy_cleanup(slurper->curlBlockData);
    BlockDestroy(slurper->block);
    free(slurper);
    slurper = NULL;
}

int SlurperSlurp(Slurper *slurper, size_t blockHeight, int verbosity)
{
    CURLcode curlResult;
    char rpcQueryGetblockhash[RPC_QUERY_SIZE_GETBLOCKHASH];
    char rpcQueryGetblock[RPC_QUERY_SIZE_GETBLOCK];
    BlockReset(slurper->block);
    // getblockhash
    BuildRPCQueryGetblockhash(rpcQueryGetblockhash, blockHeight);
    curl_easy_setopt(slurper->curlBlockHash, CURLOPT_POSTFIELDS, rpcQueryGetblockhash);
    curlResult = curl_easy_perform(slurper->curlBlockHash);
    if(curlResult != CURLE_OK) {
        fprintf(stderr, "curlResult = curl_easy_perform(slurper->curlBlockHash); failed: %s\n", curl_easy_strerror(curlResult));
        return 1;
    }
    slurper->block->m_blockHeight = blockHeight;
    BlockUpdateHash(slurper->block);
    // getblock
    BuildRPCQueryGetblock(rpcQueryGetblock, slurper->block->m_blockHash, verbosity);
    curl_easy_setopt(slurper->curlBlockData, CURLOPT_POSTFIELDS, rpcQueryGetblock);
    curlResult = curl_easy_perform(slurper->curlBlockData);
    if(curlResult != CURLE_OK) {
        fprintf(stderr, "curlResult = curl_easy_perform(slurper->curlBlockData); failed: %s\n", curl_easy_strerror(curlResult));
        return 1;
    }
    return 0;
}
