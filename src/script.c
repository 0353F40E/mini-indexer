#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "utils.h"
#include "script.h"

size_t ExtractBytecodePattern(char *dst, char *src, size_t srcLen, extract_mode parseMode)
{
    size_t d = 0; // dst iterator
    size_t s = 0; // src iterator
    size_t cPushes = 0; // consecutive pushes
    uint8_t uc;
    while (s < srcLen) {
        uc = (uint8_t) src[s];
        if (uc <= 0x60u && uc != 0x50u) { // 0x50 is RESERVED, we treat it as executable, not as push op
            if (uc == 0x00u || uc >= 0x4fu) { // OP_N
                if (parseMode == STRIP_PUSHES) {
                    ++cPushes;
                }
                else if (parseMode == STRIP_PUSH_DATA) {
                    dst[d] = 0x51u;
                    ++d;
                }
                else if (parseMode == EXTRACT_PUSHES) {
                    dst[d] = uc;
                    ++d;
                }
                ++s;
            }
            else if (uc <= 0x4bu) { // OP_PUSHBYTES_N
                size_t len = uc;
                if (s + 1 + len > srcLen) {
                    fprintf(stderr, "warn: missing data s=%zu; uc=%02x\n", s, uc);
                    break;
                }
                if (parseMode == STRIP_PUSHES) {
                    ++cPushes;
                    ++s;
                    s += len;
                }
                else if (parseMode == STRIP_PUSH_DATA) {
                    d += Int2BinScriptNum(&dst[d], len);
                    ++s;
                    s += len;
                }
                else if (parseMode == EXTRACT_PUSHES) {
                    dst[d] = uc;
                    ++d;
                    ++s;
                    memcpy(&dst[d], &src[s], len);
                    s += len;
                    d += len;
                }
            }
            else if (uc == 0x4cu || uc == 0x4du || uc == 0x4eu) { // OP_PUSHDATA_N
                size_t sz = 1 << (uc - 0x4cu);
                if (s + 1 + sz > srcLen) {
                    fprintf(stderr, "warn: missing data s=%zu; uc=%02x\n", s, uc);
                    break;
                }
                size_t len;
                len = (size_t) Bin2UIntLE(&src[s + 1], sz);
                if (s + 1 + sz + len > srcLen) {
                    fprintf(stderr, "warn: missing data sz=%zu; len=%zu s=%zu; uc=%02x\n", sz, len, s, uc);
                    break;
                }
                if (parseMode == STRIP_PUSHES) {
                    ++cPushes;
                    ++s;
                    s += sz;
                    s += len;
                }
                else if (parseMode == STRIP_PUSH_DATA) {
                    d += Int2BinScriptNum(&dst[d], len);
                    ++s;
                    s += sz;
                    s += len;
                }
                else if (parseMode == EXTRACT_PUSHES) {
                    dst[d] = uc;
                    ++d;
                    ++s;
                    memcpy(&dst[d], &src[s], sz + len);
                    s += sz + len;
                    d += sz + len;
                }
            }
        }
        else {
            if (parseMode == STRIP_PUSHES) {
                if (cPushes > 0) {
                    d += Int2BinScriptNum(&dst[d], cPushes);
                    cPushes = 0;
                }
            }
            if (parseMode != EXTRACT_PUSHES) {
                dst[d] = uc;
                ++d;
                ++s;
            }
            else {
                ++s;
            }
        }
    }
    if (parseMode == STRIP_PUSHES && cPushes > 0) {
        d += Int2BinScriptNum(&dst[d], cPushes);
        cPushes = 0;
    }
    return d;
}

size_t CountBytecodePushes(char *src, size_t srcLen)
{
    size_t s = 0; // src iterator
    size_t countPushes = 0;
    uint8_t uc;
    while (s < srcLen) {
        uc = (uint8_t) src[s];
        if (uc <= 0x60u && uc != 0x50u) { // 0x50 is RESERVED, we treat it as executable, not as push op
            if (uc == 0x00u || uc >= 0x4fu) { // OP_N
                ++countPushes;
                ++s;
            }
            else if (uc <= 0x4bu) { // OP_PUSHBYTES_N
                size_t len = uc;
                if (s + 1 + len > srcLen) {
                    fprintf(stderr, "warn: missing data s=%zu; uc=%02x\n", s, uc);
                    break;
                }
                ++countPushes;
                ++s;
                s += len;
            }
            else if (uc == 0x4cu || uc == 0x4du || uc == 0x4eu) { // OP_PUSHDATA_N
                size_t sz = 1 << (uc - 0x4cu);
                if (s + 1 + sz > srcLen) {
                    fprintf(stderr, "warn: missing data s=%zu; uc=%02x\n", s, uc);
                    break;
                }
                size_t len;
                len = (size_t) Bin2UIntLE(&src[s + 1], sz);
                if (s + 1 + sz + len > srcLen) {
                    fprintf(stderr, "warn: missing data sz=%zu; len=%zu s=%zu; uc=%02x\n", sz, len, s, uc);
                    break;
                }
                ++countPushes;
                ++s;
                s += sz;
                s += len;
            }
        }
        else {
            ++s;
        }
    }
    return countPushes;
}

size_t GetBytecodePushedData(char *dst, char *src, size_t srcLen, size_t pushIndex, int fromBehind)
{
    if (fromBehind == 1) {
        pushIndex = CountBytecodePushes(src, srcLen) - 1 - pushIndex;
    }
    size_t d = 0; // dst iterator
    size_t s = 0; // src iterator
    size_t countPushes = 0;
    uint8_t uc;
    while (s < srcLen) {
        uc = (uint8_t) src[s];
        if (uc <= 0x60u && uc != 0x50u) { // 0x50 is RESERVED, we treat it as executable, not as push op
            if (uc == 0x00u || uc >= 0x4fu) { // OP_N
                if (pushIndex == countPushes) {
                    dst[d] = uc;
                    ++d;
                    break;
                }
                ++countPushes;
                ++s;
            }
            else if (uc <= 0x4bu) { // OP_PUSHBYTES_N
                size_t len = uc;
                if (s + 1 + len > srcLen) {
                    fprintf(stderr, "warn: missing data s=%zu; uc=%02x\n", s, uc);
                    break;
                }
                //dst[d] = uc; // skip wrapping
                ++s;
                if (pushIndex == countPushes) {
                    memcpy(&dst[d], &src[s], len);
                    d += len;
                    break;
                }
                ++countPushes;
                s += len;
            }
            else if (uc == 0x4cu || uc == 0x4du || uc == 0x4eu) { // OP_PUSHDATA_N
                size_t sz = 1 << (uc - 0x4cu);
                if (s + 1 + sz > srcLen) {
                    fprintf(stderr, "warn: missing data s=%zu; uc=%02x\n", s, uc);
                    break;
                }
                size_t len;
                len = (size_t) Bin2UIntLE(&src[s + 1], sz);
                if (s + 1 + sz + len > srcLen) {
                    fprintf(stderr, "warn: missing data sz=%zu; len=%zu s=%zu; uc=%02x\n", sz, len, s, uc);
                    break;
                }
                ++s;
                s += sz;
                if (pushIndex == countPushes) {
                    memcpy(&dst[d], &src[s], len);
                    d += len;
                    break;
                }
                ++countPushes;
                s += len;
            }
        }
        else {
            ++s;
        }
    }
    return d;
}
