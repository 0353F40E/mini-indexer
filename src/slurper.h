#include <stdint.h>

#include <curl/curl.h>

#define RPC_URL "http://bitcoin:bitcoin@127.0.0.1:8332/"

#define RPC_MAX_RESULT_SIZE_GETBLOCKHASH 1024
#define RPC_CURL_BUFFER_SIZE_GETBLOCKHASH 1024
#define RPC_QUERY_HEADER_GETBLOCKHASH "{\"jsonrpc\": \"1.0\", \"id\":\"hash\", \"method\": \"getblockhash\", \"params\": ["
#define RPC_QUERY_FOOTER_GETBLOCKHASH "] }"

#define RPC_ALLOC_CHUNK_SIZE_GETBLOCK 1*1024*1024
#define RPC_CURL_BUFFER_SIZE_GETBLOCK 8*1024*1024
#define RPC_QUERY_HEADER_GETBLOCK "{\"jsonrpc\": \"1.0\", \"id\":\"block\", \"method\": \"getblock\", \"params\": [\""
#define RPC_QUERY_SEPARATOR_GETBLOCK "\", "
#define RPC_QUERY_FOOTER_GETBLOCK "] }"

typedef struct {
    size_t m_blockHeight;
    char m_blockHash[65];
    // Storage for result of RPC getblockhash method
    char m_blockHashJSON[RPC_MAX_RESULT_SIZE_GETBLOCKHASH];
    size_t m_blockHashJSONLength;
    size_t m_maxBlockHashJSONLength;
    // Storage for result of RPC getblock method
    char *m_blockDataJSON;
    size_t m_blockDataJSONLength;
    size_t m_blockDataJSONCapacity;
} Block;

typedef struct {
    Block *block;
    CURL *curlBlockHash;
    CURL *curlBlockData;
} Slurper;

Slurper* SlurperNew();

void SlurperDestroy(Slurper *slurper);

int SlurperSlurp(Slurper *slurper, size_t blockHeight, int verbosity);
