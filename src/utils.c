#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "script.h"

size_t Bin2Hex(char *dst, char *src, size_t srcStrlen)
{
    size_t i = 0;
    for ( ; i < srcStrlen; i++) {
        sprintf(&dst[2*i], "%02x", (uint8_t)src[i]);
    }
    dst[2*i]='\0';
    return 2*i;
}

size_t Hex2Bin(char *dst, char *src, size_t srcLen)
{
    uint8_t a;
    uint8_t b;
    size_t i;
    for (i = 0; i < srcLen; i+=2) {
        a = tolower(src[i]);
        b = tolower(src[i+1]);
        if (isxdigit(a) && isxdigit(b)) {
            if (isdigit(a)) {
                a = a - 0x30u;
            }
            if (islower(a)) {
                a = a + 0x0au - 0x61u;
            }
            if (isdigit(b)) {
                b = b - 0x30u;
            }
            if (islower(b)) {
                b = b + 0x0au - 0x61u;
            }
            dst[i/2] = (a << 4) | b;
        }
        else {
            break;
        }
    }
    return i/2;
}

void ReverseBytesHex(char *dst, char *src, size_t srcStrlen)
{
    char bufa;
    char bufb;
    size_t i;
    for (i = 0; i < srcStrlen/2; i+=2) {
        bufa = src[i];
        bufb = src[i+1];
        dst[i] = src[srcStrlen - i - 2];
        dst[i+1] = src[srcStrlen - i - 1];
        dst[srcStrlen - i - 2] = bufa;
        dst[srcStrlen - i - 1] = bufb;
    }
    dst[srcStrlen]='\0';
}

void ReverseBytesBin(char *dst, char *src, size_t srcLen)
{
    char buf;
    for (size_t i = 0; i < srcLen/2; i++) {
        buf = src[i];
        dst[i] = src[srcLen - i - 1];
        dst[srcLen - i - 1] = buf;
    }
}

uint64_t Bin2UIntLE(char *src, size_t len)
{
    uint64_t number = 0ull;
    for (size_t i = 0; i < len; ++i) {
        number |= (uint64_t)(uint8_t) src[i] << i*8;
    }
    return number;
}

size_t Int2BinScriptNum(char *dst, int64_t num)
{
    size_t i = 0;
    if (num >= -1 && num <= 16) {
        dst[0] = (char)(0x50 + num);
    }
    else {
        int neg = (num < 0 ? 1 : 0);
        uint64_t unum = (neg ? -num : num);
        size_t hb = 7;
        while ((unum & (0xffull << hb*8)) == 0) {
            --hb;
        }
        dst[0] = hb+1;
        ++dst;
        for (i = 0; i <= hb; ++i) {
            dst[i] = (uint8_t)(unum >> i*8);
        }
        if (neg) {
            dst[i] |= (1u << 7);
        }
    }
    return i+1;
}
