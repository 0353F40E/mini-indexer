#include <stdint.h>

size_t Bin2Hex(char *dst, char *src, size_t srcStrlen);
size_t Hex2Bin(char *dst, char *src, size_t srcLen);
void ReverseBytesHex(char *dst, char *src, size_t srcStrlen);
void ReverseBytesBin(char *dst, char *src, size_t srcLen);
uint64_t Bin2UIntLE(char *src, size_t len);
size_t Int2BinScriptNum(char *dst, int64_t num);
