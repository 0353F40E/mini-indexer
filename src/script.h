#include <stdint.h>

typedef enum
{
    STRIP_PUSHES,
    EXTRACT_PUSHES,
    STRIP_PUSH_DATA
} extract_mode;

size_t ExtractBytecodePattern(char *dst, char *src, size_t srcLen, extract_mode parseMode);
size_t CountBytecodePushes(char *src, size_t srcLen);
size_t GetBytecodePushedData(char *dst, char *src, size_t srcLen, size_t pushIndex, int fromBehind);
